# podcast-downloader

Download podcasts from several feeds (configured in **feed.json**).
 
It apply the following transformations on downloaded MP3:
 - Format the file name according to the podcast title
 - Add date prefix to file name to have file correctly ordered in my car player :laughing:
 - Update mp3 metadata to include podcast thumbnail and additional info displayed in the player.
 
## Windows:
```
bin\podcast-converter.bat
```

## Linux:
```bash
bin/podcast-converter
```
An additional a script allows creating a cron (on Linux) to automatically dowanload every day at 21:00.

Just run:
```bash
bin/create-cron.sh
```

