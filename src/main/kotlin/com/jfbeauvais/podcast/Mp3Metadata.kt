package com.jfbeauvais.podcast

import com.mpatric.mp3agic.ID3v2
import com.mpatric.mp3agic.ID3v23Tag
import com.mpatric.mp3agic.Mp3File
import java.io.File
import java.io.IOException
import java.nio.file.Files
import java.nio.file.StandardCopyOption

class Mp3Metadata(val thumbnail: File?, val albumTag: String)

fun Mp3Metadata.updateMp3Metadata(mp3: File, episode: Episode) {
    try {
        val mp3file = Mp3File(mp3)
        val id3 = mp3file.id3v2Tag ?: ID3v23Tag()
        id3.album = albumTag
        id3.artist = episode.getArtistTag()
        thumbnail?.run { setAlbumImage(id3, thumbnail) }
        mp3file.id3v2Tag = id3
        val mp3Temp = Files.createTempFile(mp3.name, "")
        mp3file.save(mp3Temp.toString())
        Files.move(mp3Temp, mp3.toPath(), StandardCopyOption.REPLACE_EXISTING)
    } catch (e: Exception) {
        e.printStackTrace()
    }
}

private fun setAlbumImage(id3: ID3v2, thumbnail: File) {
    thumbnail.exists().run {
        try {
            id3.setAlbumImage(Files.readAllBytes(thumbnail.toPath()), "image/jpeg")
        } catch (e: IOException) {
            throw RuntimeException("Cannot read thumbnail file $thumbnail")
        }
    }
}

