package com.jfbeauvais.podcast

import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException
import java.net.URL
import java.nio.channels.Channels
import java.util.*

val appHome = System.getProperty("app.home") ?: "."

val conf = Properties().apply {
    FileInputStream(File(appHome,"config/config.properties")).use { fis -> load(fis) }
}

private var outputDir = File(conf.getProperty("podcast.output.directory"))
private val thumbnailDir = File(appHome, "thumbnails")

fun main() {
    val feeds = readFeeds()
    try {
        feeds.forEach { downloadFeed(it) }
    } finally {
        feeds.write()
    }
}

fun downloadFeed(feed: Feed) {
    println("------------------------------------------------------")
    println("Downloading feed: ${feed.name}")
    val podcast = parseFeed(feed)
    val episodes = podcast.episodes
        .filter { episode -> episode.pubDate.isAfter(feed.lastDownloadedDate) }
        .sortedBy { it.pubDate }

    val thumbnail = podcast.imageUrl?.run { download(this, thumbnailDir, feed.name) }
    val mp3Metadata = Mp3Metadata(thumbnail, feed.name)
    episodes.forEachIndexed { index, episode ->
        println(String.format("Downloading %s : %d/%d", feed.name, index + 1, episodes.size))
        download(episode.url, outputDir, episode.getTargetFileName())?.run {
            mp3Metadata.updateMp3Metadata(this, episode)
            feed.lastDownloadedDate = episode.pubDate
        }
    }
}

fun download(url: String, outputDir: File, output: String): File? {
    return try {
        val extension = url.substring(url.lastIndexOf("."))
            .replace("[?;].*".toRegex(), "")
        !outputDir.exists().let { outputDir.mkdirs() }
        val outputFile = File(outputDir, output + extension)
        val rbc = Channels.newChannel(URL(url).openStream())
        FileOutputStream(outputFile).use { fos -> fos.channel.transferFrom(rbc, 0, java.lang.Long.MAX_VALUE) }
        outputFile
    } catch (e: IOException) {
        e.printStackTrace()
        System.err.println("Cannot download file: $url")
        null
    }

}
