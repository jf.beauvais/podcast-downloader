package com.jfbeauvais.podcast

import org.jsoup.Jsoup
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException
import java.util.*

class Podcast(
    val episodes: List<Episode>,
    val imageUrl: String?
)

class Episode(
    val title: String,
    val pubDate: LocalDateTime,
    val url: String
)

fun Episode.getTargetFileName(): String {
    // remove special characters not compatible wth file system
    val fileName = title.replace("[\\\\/:*?\"<>|,']*".toRegex(), "")
    val prefix = DateTimeFormatter.ofPattern("yyyyMMdd").format(pubDate)
    return prefix + "_" + fileName
}

fun Episode.getArtistTag(): String {
    return DateTimeFormatter.ofPattern("dd-MM-yyyy").format(pubDate)
}

val pubDateFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("d MMM yyyy HH:mm:ss Z", Locale.US)

private fun parsePubDate(pubDateStr: String): LocalDateTime {
    return try {
        // remove day name because it may mismatch the day number depending on timezone
        val str = pubDateStr.replace("^.*, ".toRegex(), "")
        LocalDateTime.parse(str, pubDateFormatter)
    } catch (e: DateTimeParseException) {
        System.err.println("Unable to parse date: '$pubDateStr'")
        LocalDateTime.MIN
    }
}

fun parseFeed(feed: Feed): Podcast = Jsoup.connect(feed.url).get().run {
    val episodes = select("item").map {
        Episode(
            feed.convertTitle(it.selectFirst("title").text()),
            parsePubDate(it.selectFirst("pubDate").text()),
            it.selectFirst("content,enclosure")?.attr("url") ?: it.selectFirst("link").text()
        )
    }
    val imageUrl =
        selectFirst("channel > *|image")?.attributes()?.first { att -> att.key == "url" || att.key == "href" }?.value

    Podcast(episodes, imageUrl)
}
