package com.jfbeauvais.podcast

import com.beust.klaxon.Json
import com.beust.klaxon.JsonArray
import com.beust.klaxon.Klaxon
import java.io.File
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

val dateFormatter: DateTimeFormatter = DateTimeFormatter.ISO_DATE_TIME

class Feed(
    val name: String,
    val url: String,
    @Json("last-downloaded-date")
    var lastDownloadedDateStr: String = LocalDateTime.MIN.toString(),
    @Json("title-converters")
    val titleConverters: List<TitleConverter> = emptyList()
) {
    @Json(ignored = true)
    var lastDownloadedDate: LocalDateTime
        get() = LocalDateTime.parse(lastDownloadedDateStr, dateFormatter)
        set(value) {
            lastDownloadedDateStr = value.format(dateFormatter)
        }
}

class TitleConverter(val regex: String, val replacement: String = "")

fun Feed.convertTitle(title: String): String =
    titleConverters.fold(title) { acc, c -> acc.replace(c.regex.toRegex(), c.replacement) }

val feedFile = File(appHome,"feeds.json")

fun readFeeds(): List<Feed> = Klaxon().parseArray(feedFile) ?: emptyList()

fun List<Feed>.write() {
    // trick to pretty print - https://github.com/cbeust/klaxon/issues/98
    val sb = StringBuilder(Klaxon().toJsonString(this))
    val jsonString = (Klaxon().parser().parse(sb) as JsonArray<*>).toJsonString(true)
    feedFile.printWriter().use { out -> out.println(jsonString) }
}


