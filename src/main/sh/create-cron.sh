#!/usr/bin/env bash


BIN_DIR=$(dirname "$(readlink -f "$0")")
LOG_DIR=$(dirname ${BIN_DIR})/log
mkdir -p ${LOG_DIR}
echo "0 21 * * * $BIN_DIR/podcast-downloader >> $LOG_DIR/podcast-downloader.log 2>&1" > /tmp/poadcast-downloader.cron

crontab /tmp/poadcast-downloader.cron
rm -f /tmp/poadcast-downloader.cron

echo "crontab edited:"
crontab -l
echo "Press Enter to continue..."
read a
